<?php
/**
 * Autoload functionality for unit testing
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package password-validator
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
require_once('vendor/autoload.php');

# Password Validator
[![pipeline status](https://gitlab.com/garybell/password-validation/badges/main/pipeline.svg)](https://gitlab.com/garybell/password-validation/-/commits/main) 
[![coverage report](https://gitlab.com/garybell/password-validation/badges/main/coverage.svg)](https://gitlab.com/garybell/password-validation/-/commits/main)
[![packagist version](https://img.shields.io/packagist/v/garybell/password-validator?style=flat-square)](https://packagist.org/packages/garybell/password-validator)
![package licence](https://img.shields.io/packagist/l/garybell/password-validator?label=License&style=flat-square)
![supported PHP version](https://img.shields.io/packagist/php-v/garybell/password-validator?style=flat-square)  

Validate passwords using entropy rather than arbitrary rules.

This is a PHP port of Lane Wagner's [Go Password Validator](https://github.com/lane-c-wagner/go-password-validator)

This project can be used to front a password strength meter, or simply validate password strength on the server. Benefits:

* No stupid rules (doesn't require uppercase, numbers, special characters, etc)
* Everything is based on entropy (raw cryptographic strength of the password)
* Inspired by this [XKCD](https://xkcd.com/936/)

![XKCD Passwords](https://imgs.xkcd.com/comics/password_strength.png)

## What Entropy Value Should I Use?

It's up to you. That said, here is a pretty good graph that shows some timings for different values:

![entropy](https://external-preview.redd.it/rhdADIZYXJM2FxqNf6UOFqU5ar0VX3fayLFpKspN8uI.png?auto=webp&s=9c142ebb37ed4c39fb6268c1e4f6dc529dcb4282)

Somewhere in the 50-70 range seems "average"

## Installation and Usage
Install the library with composer:  
`composer require garybell/password-validator`

To use the functionality call any of the functions within the class using the password as a parameter. 
All functions are static, so don't need the PasswordValidator object creating prior to use.

Examples:
```php
// Get the base of the password (characters from different character sets used)
$base = GaryBell\PasswordValidator::getBase($password);

// get the length of the password (characters used (only allows 2 of any single character)
$length = GaryBell\PasswordValidator::getLength($password);

// get the entropy of the password
$entropy = GaryBell\PasswordValidator::getEntropy($password);
```

The `getEntropy` functionality has an optional parameter of `decimalPlaces`, to determine the accuracy of the entropy. 
This is 2 decimal places by default. To reduce this to 1 decimal place, use:

```php
$entropy = GaryBell\PasswordValidator::getEntropy($password, 1);
```

Similarly, for 4 decimal places, use:

```php
$entropy = GaryBell\PasswordValidator::getEntropy($password, 4);
```

### Version 0.x and 1.x
Version 0.x and 1.x are no longer supported. They can still be used (so for those running PHP 7.3). 
The latest release for the 1.x version is [1.0.1](https://gitlab.com/garybell/password-validation/-/tags/1.0.1). 
Usage instructions are available [via the wiki](https://gitlab.com/garybell/password-validation/-/wikis/1.x-Usage)

## How It Works

First, we determine the "base" number. The base is a sum of the different "character sets" found in the password.

The current character sets include:

* 26 lowercase letters
* 26 uppercase
* 10 digits
* 32 special characters - ` !"#$%&'()*+,-./:;<=>?@[\]^_{|}~`

Using at least one character from each set your base number will be 94: `26+26+10+32 = 94`

Every unique character that doesn't match one of those sets will add `1` to the base.

If you only use, for example, lowercase letters and numbers, your base will be 36: `26+10 = 36`.

After we have calculated a base, the total number of brute-force-guesses is found using the following formulae: `base^length`

A password using base 26 with 7 characters would require `26^7`, or `8031810176` guesses.

Once we know the number of guesses it would take, we can calculate the actual entropy in bits using `ln(guesses)`. 

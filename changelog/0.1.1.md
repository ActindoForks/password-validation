# Version 0.1.1

This release includes a few minor updates. No functionality changes are included.

* [#1](https://gitlab.com/garybell/password-validation/-/issues/1) Remove `phpunit.result.cache` from files. 
This is created when running unit tests locally.
* [#7](https://gitlab.com/garybell/password-validation/-/issues/7) Fix pipeline auto-release. 
This was missing a CI/CD pipeline variable, and the URL for the project in the `.gitlab-ci.yml` file was incorrect.
* [#3](https://gitlab.com/garybell/password-validation/-/issues/3) Add keywords to package.json. 
This will allow the password to be discovered more easily within packagist.
* [#6](https://gitlab.com/garybell/password-validation/-/issues/6) Add pipeline code for deploying to packagist.

## Additional Notes
Due to issue #7, version 0.1.0 was not actually tagged, or officially released.

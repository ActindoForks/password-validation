# Version 2.2.0
<!-- This is a stub file. It exists to allow changelogs to be added when working on items for the above version !-->
* [#22](https://gitlab.com/garybell/password-validation/-/issues/22) Update badges in `README.md` file as follows:
  * Packagist version badge now links to Packagist page for the repository
  * Licence badge is now just an image
  * Supported PHP versions badge is now just an image
* [#24](https://gitlab.com/garybell/password-validation/-/issues/24) Add list of passwords which will always return entropy 0.  
This is made up of passwords which have been hacked from various sources, and compiled into overly used/bad passwords.

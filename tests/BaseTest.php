<?php
/**
 * DESCRIPTION
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package password-validator
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GaryBell\PasswordValidator\Tests;


use GaryBell\PasswordValidator\PasswordValidator;

class BaseTest extends \PHPUnit\Framework\TestCase
{
    public function testBaseOnlyLower()
    {
        $password = "abcd";
        $this->assertEquals(26, PasswordValidator::getBase($password));
    }

    public function testBaseOnlyUpper()
    {
        $password = "JHG";
        $this->assertEquals(26, PasswordValidator::getBase($password));
    }

    public function testBaseOnlyNumbers()
    {
        $password = "2345678";
        $this->assertEquals(10, PasswordValidator::getBase($password));
    }

    public function testBaseOnlySpecial()
    {
        $password = "%^&*(";
        $this->assertEquals(32, PasswordValidator::getBase($password));
    }

    public function testBaseUpperAndLower()
    {
        $password = "abcFGH";
        $this->assertEquals(52, PasswordValidator::getBase($password));
    }

    public function testBaseXKCD()
    {
        $password = "correct horse battery staple";
        $this->assertEquals(58, PasswordValidator::getBase($password));
    }
}

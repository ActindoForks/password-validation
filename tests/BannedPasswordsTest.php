<?php
/**
 * DESCRIPTION
 *
 * (c) 2021 Gary Bell <gary@garybell.co.uk>
 *
 * @package tanuki-tuesday
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GaryBell\PasswordValidator\Tests;


use GaryBell\PasswordValidator\PasswordValidator;

class BannedPasswordsTest extends \PHPUnit\Framework\TestCase
{
    public function testBannedPasswords()
    {
        $bannedList = PasswordValidator::bannedPasswords();
        $this->assertTrue(is_array($bannedList));
        $this->assertGreaterThan(1000, sizeof($bannedList));
        // check some random values we expect to exist
        $this->assertTrue(in_array('qwertyuiop', $bannedList));
        $this->assertTrue(in_array('q1w2e3r4t5', $bannedList));
        $this->assertTrue(in_array('midnight', $bannedList));
        $this->assertTrue(in_array('qazwsxedc', $bannedList));
        $this->assertTrue(in_array('4815162342', $bannedList));
        $this->assertTrue(in_array('poohbear', $bannedList));
        $this->assertTrue(in_array('jobandtalent', $bannedList));
        $this->assertTrue(in_array('x4ivyga51f', $bannedList));
        $this->assertTrue(in_array('1qaz2wsx3edc', $bannedList));
    }
}
